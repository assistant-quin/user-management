defmodule UserManagementTest do
  use ExUnit.Case

  @public_api_url System.get_env("KRATOS_PUBLIC_URL", "http://localhost:4433")

  @registration_start "#{@public_api_url}/self-service/registration/api"
  @registration_commit "#{@public_api_url}/self-service/registration"

  test "starts registration process" do
    resp = Req.get!(@registration_start)
    assert resp.status == 200
    assert resp.body["id"]
  end

  test "registers user and starts session" do
    flow_id = Req.get!(@registration_start).body["id"]
    body = %{
      method: "password",
      password: "GodotRocks!",
      csrf_token: "",
      traits: %{
        email: "registration_test_#{UUID.uuid1()}@test.com"
      }
    } |> Poison.encode!

    resp = Req.post!("#{@registration_commit}?flow=#{flow_id}", body: body, headers: %{ "content-type" => "application/json"})

    assert resp.status == 200
    assert resp.body["session_token"]
  end
end
