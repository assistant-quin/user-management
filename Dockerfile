FROM docker.io/elixir:otp-25-alpine as test

ENV MIX_ENV test
WORKDIR /test

ADD mix.exs mix.lock /test/

RUN mix local.hex --force
RUN mix deps.get
RUN mix deps.compile
