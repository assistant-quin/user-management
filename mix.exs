defmodule UserManagement.MixProject do
  use Mix.Project

  def project do
    [
      app: :user_management,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:req, "~> 0.4.0"},
      {:poison, "~> 5.0"},
      {:uuid, "~> 1.1"}
    ]
  end
end
