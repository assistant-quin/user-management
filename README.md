# Quin user management

- [Ory Kratos](https://www.ory.sh/docs/kratos/ory-kratos-intro)

## Run

### Start the server

```bash
docker-compose up kratos
```

### Sample request

```bash
http :4433/self-service/registration
```

## Run tests

```bash
docker-compose up test
```
